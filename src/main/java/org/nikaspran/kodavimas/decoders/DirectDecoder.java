package org.nikaspran.kodavimas.decoders;

import java.util.Iterator;

import org.nikaspran.kodavimas.model.BitList;

/**
 * Tiesioginis dekoders (angl. direct decoder)
 * 
 * @author Nikas
 * 
 */
public class DirectDecoder {

    public static final int MEMORY_SIZE = 6; // m
    private static final int MDE_SIZE = 6;

    private static final int[] SUMMATOR_BIT_INDICES = new int[] { 1, 4, 5 };
    private static final int[] MDE_BIT_INDICES = new int[] { 0, 3, 5 };

    /**
     * Dekoduoja bitų sąrašą (žinutę) tiesioginio dekodavimo algoritmu
     * 
     * @param message
     *            bitų sąrašas (žinutė)
     * @return dekoduotas bitų sąrašas
     */
    public static BitList decode(BitList message) {
        BitList mde = new BitList(MDE_SIZE);
        BitList memory = new BitList(MEMORY_SIZE);
        BitList result = new BitList();

        Iterator<Boolean> iter = message.getBits().iterator();

        // praleidžiame MEMORY_SIZE kiekį bitų, nes jie atspindi dekoderio būseną, o ne žinutę
        for (int i = 0; i < memory.length(); i++) {
            boolean input = iter.next();
            boolean control = iter.next();
            iterateDecoder(input, control, memory, mde);
        }

        // vykdome tiesioginio dekodavimo algoritmo žingsnį, kol turime likusių žinutės bitų
        while (iter.hasNext()) {
            boolean input = iter.next();
            boolean control = iter.next();

            boolean out = iterateDecoder(input, control, memory, mde);
            result.add(out);
        }

        return result;
    }

    /**
     * Vykdo tiesioginio dekodavimo algoritmo žingsnį ir grąžina dekoduotą bitą
     * 
     * @param input
     *            žinutės informacijos bitas
     * @param control
     *            kontrolinis žinutės bitas
     * @param memory
     *            dabartinė dekoderio atminties būsena
     * @param mde
     *            dabartinė dekoderio daugumos sprendimo elemento (angl. majority-decision element) būsena
     * @return dekoduotas bitas
     */
    private static boolean iterateDecoder(boolean input, boolean control, BitList memory, BitList mde) {
        boolean summatorOutput = memory.getLast();
        boolean toMde = control ^ sumOverMemory(input, memory);
        boolean mdeOutput = getMajority(toMde, mde);

        memory.shiftRightAndPush(input);
        mde.shiftRightAndPush(toMde);

        return summatorOutput ^ mdeOutput;
    }

    /**
     * Sudeda moduliu 2 algoritme nurodytose pozicijose atmintyje esančias reikšmes ir įvesties bitą
     * 
     * @param input
     *            įvesties bitas
     * @param memory
     *            dekoderio atminties būsena
     * @return nurodytų atmintyje esančių bitų ir įvesties bito suma moduliu 2
     */
    private static boolean sumOverMemory(boolean input, BitList memory) {
        boolean result = input;
        for (int bitIndex : SUMMATOR_BIT_INDICES) {
            result ^= memory.get(bitIndex);
        }
        return result;
    }

    /**
     * Vykdoma daugumos sprendimo elemento (angl. majority-decision element) operacija - grąžinamas vienetas tik jei jis sudaro daugiau nei pusę algoritme nurodytų įvesties bitų
     * 
     * @param toMde
     *            bitas, ateinantis iš algoritmo
     * @param mde
     *            daugumos sprendimo elemento būsena
     * @return daugumos sprendimo elemento operacijos rezultatas
     */
    private static boolean getMajority(boolean toMde, BitList mde) {
        int zeroes = toMde ? 0 : 1;
        int ones = toMde ? 1 : 0;
        for (int mdeIndex : MDE_BIT_INDICES) {
            if (mde.get(mdeIndex)) {
                ones++;
            } else {
                zeroes++;
            }
        }
        return ones > zeroes;
    }
}
