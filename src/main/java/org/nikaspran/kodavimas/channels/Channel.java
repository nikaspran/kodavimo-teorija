package org.nikaspran.kodavimas.channels;

import java.util.Random;

import org.nikaspran.kodavimas.model.BitList;

/**
 * Nepatikimą kanalą simuliuojanti klasė
 * 
 * @author Nikas Praninskas
 * 
 */
public class Channel {

    private static final Random randomGenerator = new Random();

    /**
     * Simuliuojamas žinutės persiuntimas nepatikimu kanalu. Funkcija apverčia atsitiktinius bitus {@link BitList} žinutėje, atsižvelgiant į klaidos tikimybę (kiekvienas bitas turi
     * <i>errorChance</i>/1 tikimybę būti apverstu)
     * 
     * @param message
     *            žinutė, kurią norime "persiųsti" kanalu
     * @param errorChance
     *            tikimybė, kad bus apverstas bitas (tarp 0.0 ir 1.0)
     * @return originalios žinutės kopiją, kurioje dalis bitų yra apversta, atsižvelgiant į klaidos tikimybę
     */
    public static BitList transmit(BitList message, double errorChance) {
        assert errorChance >= 0;
        assert errorChance <= 1;

        BitList result = new BitList(message);
        for (int i = 0; i < result.length(); i++) {
            if (randomGenerator.nextDouble() <= errorChance) {
                result.invert(i);
            }
        }

        return result;
    }

}
