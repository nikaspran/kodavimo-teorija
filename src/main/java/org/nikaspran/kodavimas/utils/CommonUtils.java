package org.nikaspran.kodavimas.utils;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

/**
 * Klasė, kurioje sudėti dažniau programoje naudojami pagalbiniai metodai
 * 
 * @author Nikas Praninskas
 * 
 */
public final class CommonUtils {

    /**
     * Sukuriamas naujas paveiksliukas {@link BufferedImage} iš pikselių masyvo paimant iš kito paveiksliuko antraštes (angl. headers)
     * 
     * @param oldImage
     *            paveiksliukas, iš kurio paimamos antraštės
     * @param pixels
     *            naujo paveiksliuko pikselių masyvas
     * @return naują paveiksliuką, kurio pikseliai yra tokie, kokie duoti metodui, o antraštės paimtos iš kito paveiksliuko
     * @throws IOException
     *             jei nepavyksta sukurti naujo paveiksliuko iš duotų parametrų
     */
    public static BufferedImage createNewImageFromPixelData(BufferedImage oldImage, byte[] pixels) throws IOException {
        DataBufferByte dataBuffer = new DataBufferByte(pixels, oldImage.getWidth() * oldImage.getHeight());

        WritableRaster originalRaster = oldImage.getRaster();
        ColorModel cm = oldImage.getColorModel();
        WritableRaster raster = Raster.createWritableRaster(originalRaster.getSampleModel(), dataBuffer, new Point(0, 0));
        return new BufferedImage(cm, raster, cm.isAlphaPremultiplied(), null);
    }

    /**
     * Grąžinamas baito bitų sąrašas, pradedant nuo žemiausio lygmens (angl. lowest-order) bito (pvz.: baito 11001000 bitų sąrašas yra [0, 0, 0, 1, 0, 0, 1, 1])
     * 
     * @param sourceByte
     *            baitas, kurio bitų sąrašą norime gauti
     * @return bitų sąrašas
     */
    public static List<Boolean> byteToBooleans(byte sourceByte) {
        List<Boolean> result = new ArrayList<Boolean>(Byte.SIZE);
        for (int i = 0; i < Byte.SIZE; i++) {
            result.add((sourceByte & 1) == 1);
            sourceByte >>= 1;
        }
        return result;
    }

    /**
     * Randa skirtumų tarp dviejų simbolių eilučių {@link String} indeksus
     * 
     * @param s1
     *            pirmoji simbolių eilutė
     * @param s2
     *            antroji simbolių eilutė
     * @return skirtumų indeksus
     */
    public static int[] indicesOfDifferences(String s1, String s2) {
        List<Integer> result = new ArrayList<Integer>();

        String longer = s1.length() > s2.length() ? s1 : s2;
        String shorter = longer == s1 ? s2 : s1;

        for (int i = 0; i < shorter.length(); i++) {
            if (shorter.charAt(i) != longer.charAt(i)) {
                result.add(i);
            }
        }

        return ArrayUtils.toPrimitive(result.toArray(new Integer[] {}));
    }

    /**
     * Privatus konstruktorius, kad klasės nebūtų galima sukurti
     */
    private CommonUtils() {
    }
}
