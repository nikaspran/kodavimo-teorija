package org.nikaspran.kodavimas;

import java.awt.EventQueue;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.JTextComponent;

import net.miginfocom.swing.MigLayout;

import org.apache.commons.lang.StringUtils;
import org.nikaspran.kodavimas.swing.BitsTab;
import org.nikaspran.kodavimas.swing.PictureTab;
import org.nikaspran.kodavimas.swing.TextTab;

/**
 * Kodavimo teorija - A15, MIF VU 2012
 * 
 * @author Nikas Praninskas
 * 
 */
public class Application {

    private JFrame mainFrame;

    private JSpinner errorField;

    /**
     * Pagrindinis programos metodas, kuris sukuria ir paleidžia grafinę vartotojo sąsają
     * 
     * @param args
     *            <i>nenaudojami</i> programos argumentai
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.mainFrame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Programos konstruktorius, kuris kviečia vartotojo sąsajos elementų sukūrimo funkciją
     */
    public Application() {
        initialize();
    }

    /**
     * Sukuriami programos vartotojo sąsajos komponentai
     */
    private void initialize() {
        mainFrame = new JFrame();
        mainFrame.setTitle("KT-A15: Nikas Praninskas");
        mainFrame.setBounds(100, 100, 966, 571);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.getContentPane().setLayout(new MigLayout("", "[right][grow]", "[grow][]"));

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        mainFrame.getContentPane().add(tabbedPane, "cell 0 0 2 1,grow");

        errorField = new JSpinner(new SpinnerNumberModel(0.05, 0.0, 1.0, 0.01));
        mainFrame.getContentPane().add(errorField, "cell 1 1,growx");

        tabbedPane.addTab("Bits", null, new BitsTab(errorField), null);
        tabbedPane.addTab("Text", null, new TextTab(errorField), null);
        tabbedPane.addTab("Picture", null, new PictureTab(errorField), null);

        mainFrame.getContentPane().add(new JLabel("Error chance:"), "cell 0 1");

    }

    /**
     * Tikrina, ar tekstinis laukas nėra tuščias, o jei laukas yra tuščias, parodo klaidos pranešimą
     * 
     * @param field
     *            tekstinis laukas
     * @param fieldName
     *            lauko pavadinimas klaidos pranešimui
     * @return ar laukas nėra tuščias
     */
    public static boolean isNotEmpty(JTextComponent field, String fieldName) {
        if (!StringUtils.isNotBlank(field.getText())) {
            JOptionPane.showMessageDialog(field.getParent(), fieldName + " must not be empty");
            return false;
        }
        return true;
    }

    /**
     * Tikrina, ar tekstiniame lauke įrašytos tik leidžiami simboliai, o jei yra neleidžiamų, parodo klaidos pranešimą
     * 
     * @param field
     *            tekstinis laukas
     * @param fieldName
     *            lauko pavadinimas klaidos pranešimui
     * @param allowedCharacters
     *            leidžiami simboliai
     * @return ar tekstiniame lauke yra tik leidžiami simboliai
     */
    public static boolean isAllowedCharactersOnly(JTextComponent field, String fieldName, char... allowedCharacters) {
        if (!StringUtils.containsOnly(field.getText(), allowedCharacters)) {
            JOptionPane.showMessageDialog(field.getParent(), fieldName + " can only contain these characters: " + Arrays.toString(allowedCharacters));
            return false;
        }
        return true;
    }

}
