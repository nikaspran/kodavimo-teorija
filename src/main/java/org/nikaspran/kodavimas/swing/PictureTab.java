package org.nikaspran.kodavimas.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;

import net.miginfocom.swing.MigLayout;

import org.nikaspran.kodavimas.channels.Channel;
import org.nikaspran.kodavimas.decoders.DirectDecoder;
import org.nikaspran.kodavimas.encoders.ConvolutionalEncoder;
import org.nikaspran.kodavimas.model.BitList;
import org.nikaspran.kodavimas.utils.CommonUtils;

/**
 * Vartotojo interfeiso dalis, kurioje galima atlikti veiksmus su paveiksliukais (trečiasis scenarijus)
 * 
 * @author Nikas Praninskas
 * 
 */
public class PictureTab extends JPanel {
    private static final long serialVersionUID = -804392859318331029L;

    private ImagePanel originalImagePanel;
    private ImagePanel unencodedImagePanel;
    private ImagePanel encodedImagePanel;
    private JButton btnLoadPicture;
    private JButton btnTransmitPicture;

    private JSpinner errorField;

    /**
     * Sukuriamas naujas vartotojo interfeiso elementas
     * 
     * @param errorField
     *            klaidos tikimybės laukas, naudojamas gauti šią reikšmę iš vartotojo
     */
    public PictureTab(JSpinner errorField) {
        super();
        this.errorField = errorField;

        this.setLayout(new MigLayout("", "[33%][33%][34%]", "[][grow][]"));

        this.add(new JLabel("Original Picture"), "cell 0 0");

        this.add(new JLabel("Without Encoding"), "cell 1 0");

        this.add(new JLabel("With Encoding"), "cell 2 0");

        originalImagePanel = new ImagePanel();
        this.add(new JScrollPane(originalImagePanel), "cell 0 1,grow");

        unencodedImagePanel = new ImagePanel();
        this.add(new JScrollPane(unencodedImagePanel), "cell 1 1,grow");

        encodedImagePanel = new ImagePanel();
        this.add(new JScrollPane(encodedImagePanel), "cell 2 1,grow");

        // Sukuriamas paveiksliuko įkėlimo mygtukas (Load picture)
        btnLoadPicture = new JButton("Load picture");
        btnLoadPicture.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Parodomas failų pasirinkimo dialogas ir iš gauto failo ištraukiamas bei parodomas paveiksliukas, nepavykus parodoma klaida
                JFileChooser fileChooser = new JFileChooser();
                int returnVal = fileChooser.showOpenDialog(btnLoadPicture.getParent());
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    try {
                        File file = fileChooser.getSelectedFile();
                        originalImagePanel.setImage(ImageIO.read(file));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(btnLoadPicture.getParent(), "Unable to load image: " + ex.getMessage());
                    }
                }
            }
        });
        this.add(btnLoadPicture, "flowx,cell 2 2,alignx right");

        // Sukuriamas paveiksliuko perdavimo kanalu simuliacijos mygtukas (Transmit)
        btnTransmitPicture = new JButton("Transmit");
        btnTransmitPicture.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Vykdoma paveiksliuko perdavimo kanalu su ir be kodavimo simuliacija, įvykus klaidai parodomas pranešimas
                if (originalImagePanel.getImage() == null) {
                    JOptionPane.showMessageDialog(btnTransmitPicture.getParent(), "Original image must not be empty");
                    return;
                }
                try {
                    transmitImage();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(btnTransmitPicture.getParent(), "Could not decode image: " + ex.getMessage());
                }
            }
        });
        this.add(btnTransmitPicture, "cell 2 2,alignx right");
    }

    /**
     * Vykdoma paveiksliuko perdavimo nepatikimu kanalu su ir be kodavimo simuliacija bei vartotojo sąsajoje parodomi šių perdavimų rezultatai
     * 
     * @throws IOException
     *             jei įvyko klaida atkuriant paveiksliukus po perdavimo
     */
    protected void transmitImage() throws IOException {
        BufferedImage original = originalImagePanel.getImage();
        byte[] pixels = ((DataBufferByte) original.getRaster().getDataBuffer()).getData();
        BitList bits = new BitList(pixels);

        unencodedImagePanel.setImage(CommonUtils.createNewImageFromPixelData(original, Channel.transmit(bits, getError()).toByteArray()));

        BitList encodedResult = DirectDecoder.decode(Channel.transmit(ConvolutionalEncoder.encode(bits), getError()));
        encodedImagePanel.setImage(CommonUtils.createNewImageFromPixelData(original, encodedResult.toByteArray()));
    }

    /**
     * @return vartotojo įvesta kanalo klaidos tikimybė
     */
    private double getError() {
        return (Double) errorField.getValue();
    }

}
