package org.nikaspran.kodavimas.swing;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Paveiksliuką galintis parodyti komponentas vartotojo sąsajai
 * 
 * @author Nikas Praninskas
 * 
 */
public class ImagePanel extends JLabel {
    private static final long serialVersionUID = 5573887635965897111L;

    private BufferedImage image;

    /**
     * @return dabartinį komponento paveiksliuką
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * Nustatyti naują komponento paveiksliuką
     * 
     * @param image
     *            naujas paveiksliukas
     */
    public void setImage(BufferedImage image) {
        this.image = image;
        setIcon(new ImageIcon(image));
    }

}
