package org.nikaspran.kodavimas.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.text.JTextComponent;

import net.miginfocom.swing.MigLayout;

import org.apache.commons.lang.StringUtils;
import org.nikaspran.kodavimas.Application;
import org.nikaspran.kodavimas.channels.Channel;
import org.nikaspran.kodavimas.decoders.DirectDecoder;
import org.nikaspran.kodavimas.encoders.ConvolutionalEncoder;
import org.nikaspran.kodavimas.model.BitList;
import org.nikaspran.kodavimas.utils.CommonUtils;

/**
 * Vartotojo interfeiso dalis, kurioje galima atlikti veiksmus su bitais (pirmasis scenarijus)
 * 
 * @author Nikas Praninskas
 * 
 */
public class BitsTab extends JPanel {
    private static final long serialVersionUID = -215683213855963761L;

    private MarkedTextPane messageField;
    private MarkedTextPane encodedField;
    private MarkedTextPane transmittedField;
    private MarkedTextPane decodedField;
    private JButton btnEncode;
    private JButton btnTransmit;
    private JButton btnDecode;
    private JLabel bitErrorCount;

    private JSpinner errorField;

    /**
     * Sukuriamas naujas vartotojo interfeiso elementas
     * 
     * @param errorField
     *            klaidos tikimybės laukas, naudojamas gauti šią reikšmę iš vartotojo
     */
    public BitsTab(JSpinner errorField) {
        super();

        this.setLayout(new MigLayout("", "[][grow][100px:100px][]", "[][][][][][grow]"));
        this.errorField = errorField;
        messageField = new MarkedTextPane();
        this.add(messageField, "cell 1 0 2 1,growx");

        // Aprašomas žinutės užkodavimo mygtukas (Encode)
        btnEncode = new JButton("Encode");
        btnEncode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (checkBitPreconditions(messageField, "Message", null)) {
                    messageField.setText(new BitList(messageField.getText()).toString());
                    encodedField.setText(ConvolutionalEncoder.encode(new BitList(messageField.getText())).toString());
                }
            }
        });
        this.add(btnEncode, "cell 3 0,growx");

        this.add(new JLabel("Encoded message:"), "cell 0 1,alignx trailing");

        this.add(new JLabel("Message:"), "cell 0 0,alignx trailing");

        this.add(new JLabel("Errors:"), "cell 0 4,alignx right");

        encodedField = new MarkedTextPane();
        this.add(encodedField, "flowx,cell 1 1 2 1,growx");

        bitErrorCount = new JLabel("");
        this.add(bitErrorCount, "cell 1 4");

        this.add(new JLabel("Transmitted message:"), "cell 0 2,alignx trailing");

        transmittedField = new MarkedTextPane();
        this.add(transmittedField, "cell 1 2 2 1,growx");

        // Aprašomas žinutės dekodavimo mygtukas (Decode)
        btnDecode = new JButton("Decode");
        btnDecode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Persiųstos žinutės ilgis turi būti toks pats, kaip užkoduotos
                if (checkBitPreconditions(transmittedField, "Transmitted message", StringUtils.deleteWhitespace(encodedField.getText()).length())) {
                    decodeBits();
                }
            }
        });
        this.add(btnDecode, "cell 3 2,growx");

        this.add(new JLabel("Decoded message:"), "cell 0 3,alignx trailing");

        decodedField = new MarkedTextPane();
        this.add(decodedField, "cell 1 3 2 1,growx");

        // Aprašomas žinutės siuntimo mygtukas (Transmit)
        btnTransmit = new JButton("Transmit");
        btnTransmit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Užkoduotos žinutės ilgis priklauso nuo originalios žinutės ilgio, turime jį tikrinti
                int expectedLength = (StringUtils.deleteWhitespace(messageField.getText()).length() + DirectDecoder.MEMORY_SIZE) * ConvolutionalEncoder.OUT_SIZE;
                if (checkBitPreconditions(encodedField, "Encoded message", expectedLength)) {
                    transmitBits();
                }
            }
        });
        this.add(btnTransmit, "cell 3 1,growx");
    }

    /**
     * Simuliuojamas bitų persiuntimas kanalu ir pateikiami veiksmo rezultatai vartotojo sąsajoje, pažymimos siuntime įvykusios klaidos
     */
    protected void transmitBits() {
        String result = Channel.transmit(new BitList(encodedField.getText()), getError()).toString();
        transmittedField.setText(result);
        transmittedField.markText(CommonUtils.indicesOfDifferences(encodedField.getText(), result));
    }

    /**
     * Dekoduojama persiųsta žinutė, pateikiami veiksmo rezultatai vartotojo sąsajoje, pažymimos klaidos tarp dekoduotos ir originalios žinučių, suskaičiuojamas ir parodomas klaidų skaičius
     */
    protected void decodeBits() {
        String result = DirectDecoder.decode(new BitList(transmittedField.getText())).toString();
        decodedField.setText(result);
        int[] errorIndices = CommonUtils.indicesOfDifferences(messageField.getText(), result);
        decodedField.markText(errorIndices);
        bitErrorCount.setText(String.valueOf(errorIndices.length));
    }

    /**
     * Patikrinama, ar vartotojas tinkamai įvedė reikšmes ir, jei netinkamai, parodomas klaidos pranešimas
     * 
     * @param field
     *            tekstinis laukas, kuris yra tikrinamas
     * @param fieldName
     *            tekstinio lauko pavadinimas klaidos pranešimams
     * @param characterCount
     *            tekstiniame lauke turintis būti simbolių skaičius (be tarpų)
     * @return ar lauko reikšmė tinkama
     */
    public boolean checkBitPreconditions(JTextComponent field, String fieldName, Integer characterCount) {
        if (!Application.isNotEmpty(field, fieldName) || !Application.isAllowedCharactersOnly(field, fieldName, '0', '1', ' ')) {
            return false;
        }

        if (characterCount != null && StringUtils.deleteWhitespace(field.getText()).length() != characterCount) {
            JOptionPane.showMessageDialog(field.getParent(), fieldName + " must contain exactly " + characterCount + " characters");
            return false;
        }

        return true;
    }

    /**
     * @return vartotojo įvesta kanalo klaidos tikimybė
     */
    private double getError() {
        return (Double) errorField.getValue();
    }
}
