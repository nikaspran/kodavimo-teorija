package org.nikaspran.kodavimas.swing;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 * Tekstinis laukas, kuriame raudona spalva pažymimos nurodytos pozicjos
 * 
 * @author Nikas Praninskas
 * 
 */
public class MarkedTextPane extends JTextPane {
    private static final long serialVersionUID = 5018936304005030304L;

    private static final String REGULAR_STYLE = "regular";
    private static final String MARKED_STYLE = "marked";

    private Color markedColor = Color.RED;

    /**
     * Komponento konstruktorius, kuriame sukuriamas komponentas ir pridedami reikalingi teksto stiliai
     */
    public MarkedTextPane() {
        super();
        addStylesToDocument();
    }

    /**
     * Raudonai pažymimos nurodytos teksto pozicijos
     * 
     * @param positions
     *            teksto pozicijos, kurias norime pažymėti
     */
    public void markText(int... positions) {
        StyledDocument doc = getStyledDocument();
        clearMarkings();
        for (int pos : positions) {
            doc.setCharacterAttributes(pos, 1, doc.getStyle(MARKED_STYLE), true);
        }
    }

    /**
     * Išvalomi teksto pažymėjimai
     */
    private void clearMarkings() {
        getStyledDocument().setCharacterAttributes(0, getText().length(), SimpleAttributeSet.EMPTY, true);
    }

    /**
     * Sukuriami ir pridedami pažymėjimui reikalingi stiliai
     */
    private void addStylesToDocument() {
        Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        Style regular = getStyledDocument().addStyle(REGULAR_STYLE, defaultStyle);

        Style marked = getStyledDocument().addStyle(MARKED_STYLE, regular);
        StyleConstants.setForeground(marked, markedColor);
    }
}
