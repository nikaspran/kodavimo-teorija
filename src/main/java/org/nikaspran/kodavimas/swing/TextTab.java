package org.nikaspran.kodavimas.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextPane;

import net.miginfocom.swing.MigLayout;

import org.nikaspran.kodavimas.Application;
import org.nikaspran.kodavimas.channels.Channel;
import org.nikaspran.kodavimas.decoders.DirectDecoder;
import org.nikaspran.kodavimas.encoders.ConvolutionalEncoder;
import org.nikaspran.kodavimas.model.BitList;
import org.nikaspran.kodavimas.utils.CommonUtils;

/**
 * Vartotojo interfeiso dalis, kurioje galima atlikti veiksmus su tekstu (antrasis scenarijus)
 * 
 * @author Nikas Praninskas
 * 
 */
public class TextTab extends JPanel {
    private static final long serialVersionUID = 144416528232728209L;

    private JTextPane originalTextArea;
    private JButton transmitTextButton;
    private MarkedTextPane unencodedTextArea;
    private MarkedTextPane encodedTextArea;
    private JLabel textEncodedErrorCount;
    private JLabel textUnencodedErrorCount;

    private JSpinner errorField;

    /**
     * Sukuriamas naujas vartotojo interfeiso elementas
     * 
     * @param errorField
     *            klaidos tikimybės laukas, naudojamas gauti šią reikšmę iš vartotojo
     */
    public TextTab(JSpinner errorField) {
        super();
        this.errorField = errorField;
        this.setLayout(new MigLayout("", "[50%][50%]", "[][grow][][grow][]"));

        this.add(new JLabel("Original text"), "cell 0 0");

        this.add(new JLabel("Without encoding"), "flowx,cell 1 0");

        originalTextArea = new JTextPane();
        this.add(new JScrollPane(originalTextArea), "cell 0 1 1 3,grow");

        unencodedTextArea = new MarkedTextPane();
        unencodedTextArea.setEditable(false);
        this.add(new JScrollPane(unencodedTextArea), "cell 1 1,grow");

        this.add(new JLabel("With encoding"), "flowx,cell 1 2");

        // Sukuriamas teksto perdavimo simuliacijos mygtukas (Transmit)
        transmitTextButton = new JButton("Transmit");
        transmitTextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Jei tekstas tuščias, parodomas klaidos pranešimas
                if (Application.isNotEmpty(originalTextArea, "Original text")) {
                    transmitText();
                }
            }
        });

        encodedTextArea = new MarkedTextPane();
        encodedTextArea.setEditable(false);
        this.add(new JScrollPane(encodedTextArea), "cell 1 3,grow");
        this.add(transmitTextButton, "cell 1 4,alignx right");

        textEncodedErrorCount = new JLabel("");
        this.add(textEncodedErrorCount, "cell 1 2");

        textUnencodedErrorCount = new JLabel("");
        this.add(textUnencodedErrorCount, "cell 1 0");
    }

    /**
     * Vykdoma teksto perdavimo nepatikimu kanalu su ir be kodavimo simuliacija bei vartotojo sąsajoje parodomi šių perdavimų rezultatai, bei pažymimos perdavimo klaidos ir parodomas jų kiekis
     */
    protected void transmitText() {
        String message = originalTextArea.getText();
        BitList bits = new BitList(message.getBytes());

        unencodedTextArea.setText(new String(Channel.transmit(bits, getError()).toByteArray()));
        int[] unencodedErrorIndices = CommonUtils.indicesOfDifferences(unencodedTextArea.getText(), originalTextArea.getText());
        unencodedTextArea.markText(unencodedErrorIndices);
        textUnencodedErrorCount.setText(" - Errors: " + unencodedErrorIndices.length);

        BitList encodedResult = DirectDecoder.decode(Channel.transmit(ConvolutionalEncoder.encode(bits), getError()));
        encodedTextArea.setText(new String(encodedResult.toByteArray()));
        int[] encodedErrorIndices = CommonUtils.indicesOfDifferences(encodedTextArea.getText(), originalTextArea.getText());
        encodedTextArea.markText(encodedErrorIndices);
        textEncodedErrorCount.setText(" - Errors: " + encodedErrorIndices.length);
    }

    /**
     * @return vartotojo įvesta kanalo klaidos tikimybė
     */
    private double getError() {
        return (Double) errorField.getValue();
    }
}
