package org.nikaspran.kodavimas.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nikaspran.kodavimas.utils.CommonUtils;

/**
 * Klasė, kurioje įgyvendinamas neriboto ilgio bitų sąrašas bei su juo susijusios operacijos
 * 
 * @author Nikas Praninskas
 * 
 */
public class BitList {
    private static final char ONE_CHAR = '1';
    private static final char ZERO_CHAR = '0';

    private List<Boolean> bits;

    /**
     * Sukuria bitų sąrašą iš baitų masyvo
     * 
     * @param bytes
     *            baitų masyvas
     */
    public BitList(byte[] bytes) {
        this.bits = new ArrayList<Boolean>(Byte.SIZE * bytes.length);

        for (byte sourceByte : bytes) {
            this.bits.addAll(CommonUtils.byteToBooleans(sourceByte));
        }
    }

    /**
     * Sukuria nurodyto dydžio bitų sąrašą, kurio visi elementai yra nuliai
     * 
     * @param size
     *            bitų sąrašo dydis
     */
    public BitList(int size) {
        this.bits = new ArrayList<Boolean>(size);
        for (int i = 0; i < size; i++) {
            add(false);
        }
    }

    /**
     * Sukuria bitų sąrašą iš duotų būlio (angl. boolean) reikšmių
     * 
     * @param bits
     *            būlio reikšmės
     */
    public BitList(boolean... bits) {
        this.bits = new ArrayList<Boolean>(bits.length);

        for (boolean bit : bits) {
            add(bit);
        }
    }

    /**
     * Sukuria bitų sąrašą nukopijuojant kito bitų sąrašo reikšmes
     * 
     * @param other
     *            kitas bitų sąrašas
     */
    public BitList(BitList other) {
        this.bits = new ArrayList<Boolean>(other.getBits());
    }

    /**
     * Sukuria bitų sąrašą išimant visus nulius ir vienetus iš simbolių eilutės iš eilės (pvz. simbolių eilutė "110a1" paverčiama į bitų sąrašą su elementais [1, 1, 0, 1])
     * 
     * @param bits
     *            simbolių eilutė
     */
    public BitList(String bits) {
        this.bits = new ArrayList<Boolean>(bits.length());

        bits = StringUtils.deleteWhitespace(bits);
        for (int i = 0; i < bits.length(); i++) {
            if (bits.charAt(i) == ONE_CHAR) {
                add(true);
            }
            if (bits.charAt(i) == ZERO_CHAR) {
                add(false);
            }
        }
    }

    /**
     * Bitų sąrašą paslenka nurodytu skaičiumi pozicijų į dešinę (angl. right shift). Bitų sąrašo ilgis išsaugomas
     * 
     * @param n
     *            pozicijų skaičius, kuriomis norima paslinkti bitų sąrašą
     */
    public void shiftRight(int n) {
        for (int i = 0; i < n; i++) {
            bits.add(0, false);
            bits.remove(bits.size() - 1);
        }
    }

    /**
     * Bitų sąrašą paslenka viena pozicija į dešinę ir į pradinę poziciją įrašo naują reikšmę. Bitų sąrašo ilgis išsaugomas
     * 
     * @param newBit
     *            reikšmė, kuri yra įrašoma į pradinę poziciją
     */
    public void shiftRightAndPush(boolean newBit) {
        shiftRight(1);
        set(0, newBit);
    }

    /**
     * Apverčiamas bitų sąrašo bitas esantis nurodytoje pozicijoje
     * 
     * @param index
     *            pozicija, kurioje esantį bitą norima apversti
     */
    public void invert(int index) {
        set(index, !get(index));
    }

    /**
     * Grąžinamas nurodytoje bitų sąrašo pozicijoje esantis bitas
     * 
     * @param index
     *            bitų sąrašo pozicija
     * @return nurodytoje bitų sąrašo pozicijoje esantis bitas
     */
    public boolean get(int index) {
        return bits.get(index);
    }

    /**
     * Prie bitų sąrašo pabaigos pridedama nauja reikšmė (bitas)
     * 
     * @param value
     *            bito reikšmė
     */
    public void add(boolean value) {
        bits.add(value);
    }

    /**
     * Nustatoma nurodytoje bitų sąrašo pozicijoje esančio bito reikšmė
     * 
     * @param index
     *            bitų sąrašo pozicija
     * @param value
     *            reikšmė, į kurią bus pakeistas nurodytoje pozicijoje esantis bitas
     */
    public void set(int index, boolean value) {
        bits.set(index, value);
    }

    /**
     * @return bitų sąrašo ilgis
     */
    public int length() {
        return bits.size();
    }

    /**
     * @return bitų sąrašo vidinė reprezentacija
     */
    public List<Boolean> getBits() {
        return bits;
    }

    /**
     * Priskiriama nauja bitų sąrašo vidinė reprezentacija
     * 
     * @param bits
     *            naujas bitų sąrašas
     */
    public void setBits(List<Boolean> bits) {
        this.bits = bits;
    }

    /**
     * @return paskutinis bitų sąrašo elementas
     */
    public boolean getLast() {
        return get(bits.size() - 1);
    }

    /**
     * Bitų sąrašas paverčiamas į baitų masyvą (jei bitų yra mažiau, nei reikia pilnam baitui, aukščiausio lygio paskutinio baito bitai būna nuliai). Atliekamas toks vertimas, kad
     * {@link BitList#BitList(byte[])} ir ši operacija būtų suderinamos
     * 
     * @return šio bitų sąrašo reprezentacija baitų masyvu
     */
    public byte[] toByteArray() {
        int arraySize = bits.size() / Byte.SIZE + (bits.size() % Byte.SIZE > 0 ? 1 : 0);
        byte[] result = new byte[arraySize];

        byte currentByte = 0;
        for (int i = 0; i < bits.size(); i++) {
            int bitIndex = i % Byte.SIZE;

            if (bits.get(i)) {
                currentByte |= 1 << bitIndex;
            }

            if (bitIndex == Byte.SIZE - 1) {
                result[i / Byte.SIZE] = currentByte;
                currentByte = 0;
            }
        }
        return result;
    }

    /**
     * Grąžina bitų sąrašo reprezentaciją simbolių eilute
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bits.size(); i++) {
            boolean bit = get(i);
            if (i % 4 == 0 && i > 0) {
                builder.append(" ");
            }
            builder.append(bit ? ONE_CHAR : ZERO_CHAR);
        }
        return builder.toString();
    }

}
