package org.nikaspran.kodavimas.encoders;

import org.nikaspran.kodavimas.decoders.DirectDecoder;
import org.nikaspran.kodavimas.model.BitList;

/**
 * Konvoliucinis užkoduotojas (angl. convolutional encoder)
 * 
 * @author Nikas Praninskas
 * 
 */
public class ConvolutionalEncoder {

    private static final int MEMORY_SIZE = 6; // m
    public static final int BLOCK_SIZE = 1; // k
    public static final int OUT_SIZE = 2; // n

    private static final int[] SUMMATOR_BIT_INDICES = new int[] { 1, 4, 5 };

    /**
     * Užkoduoja bitų sąrašą naudojant sąsūkos (konvoliucinį) kodavimą
     * 
     * @param message
     *            bitų sąrašas (žinutė), kurį norime užkoduoti
     * @return užkoduotą bitų sąrašą
     */
    public static BitList encode(BitList message) {
        // Kai kuriuos bitus reiks praleisti, kad atmestume pradinę dekoderio būseną, todėl papildomai užkoduosime kelis nulinius bitus
        BitList extendedMessage = accountForDecoderMemorySize(message);

        BitList memory = new BitList(MEMORY_SIZE);
        BitList result = new BitList(extendedMessage.length() * OUT_SIZE);

        // Einame per žinutę ir naudojame sąsūkos kodavimo algoritmo žingsnį
        for (int i = 0; i < extendedMessage.length(); i += BLOCK_SIZE) {
            boolean input = extendedMessage.get(i);

            for (int j = 0; j < OUT_SIZE; j++) {
                boolean resultBit = j == 0 ? input : sum(input, memory);
                result.set(i * OUT_SIZE + j, resultBit);
            }
            memory.shiftRightAndPush(input);
        }

        return result;
    }

    /**
     * Prideda papildomus bitus, kuriuos reikia užkoduoti, norint atsižvelgti į dekoderio pradinės būsenos atmetimą
     * 
     * @param message
     *            pradinė žinutė
     * @return žinutė, kurios gale yra pridėta paildomų nulinių bitų, atsižvelgiant į dekoderio atminties dydį
     */
    private static BitList accountForDecoderMemorySize(BitList message) {
        BitList extended = new BitList(message);
        for (int i = 0; i < DirectDecoder.MEMORY_SIZE; i++) {
            extended.add(false);
        }
        return extended;
    }

    /**
     * Susumuoja moduliu 2 visas algoritme nurodytas atminties pozicijas bei įvesties bitą
     * 
     * @param input
     *            įvesties bitas
     * @param memory
     *            atminties būsena
     * @return sumos moduliu 2 rezultatą
     */
    private static boolean sum(boolean input, BitList memory) {
        boolean result = input;
        for (int bitIndex : SUMMATOR_BIT_INDICES) {
            result ^= memory.get(bitIndex);
        }
        return result;
    }

}
